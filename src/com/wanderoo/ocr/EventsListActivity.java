package com.wanderoo.ocr;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.wanderoo.database.CalendarEvent;

public class EventsListActivity extends Activity {
	ArrayList<String> resources = new ArrayList<String>();
	protected ArrayList<CalendarEvent> events = null;
	private EventsListArrayAdaptor listOfEvents;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.events_list);
		events = ClickEventActivity.getCalendarEvents(null);
		for (int i = 0; i < events.size(); i++) {
			resources.add(events.get(0).event_id);
		}

		listOfEvents = new EventsListArrayAdaptor(this, resources);
		ListView eventsList = (ListView) findViewById(R.id.eventsList);
		eventsList.setAdapter(listOfEvents);
		eventsList.setEmptyView(findViewById(R.id.noEvents));
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
}
