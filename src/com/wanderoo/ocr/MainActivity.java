package com.wanderoo.ocr;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;
import com.wanderoo.database.DataSource;
import com.wanderoo.ocr.language.LanguageCodeHelper;

public class MainActivity extends Activity {
	SharedPreferences prefs = null;
	private static String TAG = "MainActivity";
	protected static final String BASE_DATA_LOADED = "baseDataLoaded";
	protected static final int GET_UNIVERSITIES = 100;
	protected static boolean isFirstLaunch; // True if this is the first time the app is being run
	
	private boolean clickEventStarted = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		checkFirstLaunch();
		if (isFirstLaunch) {
			setDefaultPreferences();
			boolean baseDataLoaded = prefs.getBoolean(BASE_DATA_LOADED, false);
			if (!baseDataLoaded) {
				DownloadFile downloadFile = new DownloadFile();
				downloadFile.execute();
			}
		} else {
			resumeClickEvent();
		}
	}
	
	public void resumeClickEvent() {
		clickEventStarted=true;
		startActivity(new Intent(this, ClickEventActivity.class));
	}


	@Override
	protected void onResume() {
		super.onResume();
		if(clickEventStarted)
			finish();
	}

	/**
	   * We want the help screen to be shown automatically the first time a new version of the app is
	   * run. The easiest way to do this is to check android:versionCode from the manifest, and compare
	   * it to a value stored as a preference.
	   */
	protected boolean checkFirstLaunch() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			int currentVersion = info.versionCode;
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(this);
			int lastVersion = prefs.getInt(
					PreferencesActivity.KEY_HELP_VERSION_SHOWN, 0);
			if (lastVersion == 0) {
				isFirstLaunch = true;
			} else {
				isFirstLaunch = false;
			}
			if (currentVersion > lastVersion) {
				// Record the last version for which we last displayed the
				// What's New (Help) page
				prefs.edit()
						.putInt(PreferencesActivity.KEY_HELP_VERSION_SHOWN,
								currentVersion).commit();
				return true;
			}
		} catch (PackageManager.NameNotFoundException e) {
			Log.w(TAG , e);
		}
		return false;
	}
	
	private void loadOcrData(){
		File storageDirectory = getStorageDirectory();
	    if (storageDirectory != null) {
			initOcrEngine(storageDirectory,
					CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE,
					LanguageCodeHelper.getOcrLanguageName(this,
							CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE));
		}
	}
	
	/**
	   * Requests initialization of the OCR engine with the given parameters.
	   * 
	   * @param storageRoot Path to location of the tessdata directory to use
	   * @param languageCode Three-letter ISO 639-3 language code for OCR 
	   * @param languageName Name of the language for OCR, for example, "English"
	   */
	  private void initOcrEngine(File storageRoot, String languageCode, String languageName) {    
	    int ocrEngineMode = TessBaseAPI.OEM_TESSERACT_ONLY;

	    // Start AsyncTask to install language data and init OCR
	    CaptureActivity.baseApi = new TessBaseAPI();
	    new OcrInitAsyncTask(this, CaptureActivity.baseApi, null, null, languageCode, languageName, ocrEngineMode)
	      .execute(storageRoot.toString());
	    /*baseApi.init(storageRoot.toString() + File.separator, languageCode, ocrEngineMode);
	    resumeOCR();
	    showLanguageName();*/
	  }
	
	/** Finds the proper location on the SD card where we can save files. */
	  private File getStorageDirectory() {
	    //Log.d(TAG, "getStorageDirectory(): API level is " + Integer.valueOf(android.os.Build.VERSION.SDK_INT));
	    
	    String state = null;
	    try {
	      state = Environment.getExternalStorageState();
	    } catch (RuntimeException e) {
	      Log.e(TAG, "Is the SD card visible?", e);
	      showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable.");
	    }
	    
	    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

	      // We can read and write the media
	      //    	if (Integer.valueOf(android.os.Build.VERSION.SDK_INT) > 7) {
	      // For Android 2.2 and above
	      
	      try {
	        return getExternalFilesDir(Environment.MEDIA_MOUNTED);
	      } catch (NullPointerException e) {
	        // We get an error here if the SD card is visible, but full
	        Log.e(TAG, "External storage is unavailable");
	        showErrorMessage("Error", "Required external storage (such as an SD card) is full or unavailable.");
	      }
	      
	      //        } else {
	      //          // For Android 2.1 and below, explicitly give the path as, for example,
	      //          // "/mnt/sdcard/Android/data/com.wanderoo.ocr/files/"
	      //          return new File(Environment.getExternalStorageDirectory().toString() + File.separator + 
	      //                  "Android" + File.separator + "data" + File.separator + getPackageName() + 
	      //                  File.separator + "files" + File.separator);
	      //        }
	    
	    } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
	    	// We can only read the media
	    	Log.e(TAG, "External storage is read-only");
	      showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable for data storage.");
	    } else {
	    	// Something else is wrong. It may be one of many other states, but all we need
	      // to know is we can neither read nor write
	    	Log.e(TAG, "External storage is unavailable");
	    	showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable or corrupted.");
	    }
	    return null;
	  }

	private class DownloadFile extends AsyncTask<String, Integer, String> {
		String[] location = null;
		DataSource dataSource = new DataSource(getApplicationContext());
		BufferedReader reader = null;
		Date date = new Date();
		String line = null;
		ArrayList<String> univs = new ArrayList<String>();
		StringBuilder dt = new StringBuilder(ClickEventActivity.format.format(date));
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			loadOcrData();
		}
		@Override
	    protected String doInBackground(String... sUrl) {
	        try {
	        	HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet();
				dt.reverse().append("NLS");
			    request.setURI(new URI("http://clickevent-v2.appspot.com/universitylist?key="
						+ dt));
				HttpResponse response = client.execute(request);
				reader = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				//first line is number of locations
				reader.readLine();
				while ((line = reader.readLine()) != null) {
					Log.i("university", line);
					String str[] = line.split("\\|");
					univs.add(str[1]);
				}
	        	
	            dataSource.open();
	        	for(String univ:univs){
					request.setURI(new URI(
							"http://clickevent-v2.appspot.com/clickeventjava?key="
									+ dt + "&col=" + univ));
		            response = client.execute(request);
		            reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		            // download the file
		            String line = null;
		            while((line = reader.readLine())!=null && !line.equals("")){
		            	location = line.split("\\|");
		            	dataSource.createLocation(location[0], location[1], location[2]);
		            }
	        	}
	        	prefs.edit().putBoolean(BASE_DATA_LOADED, true).commit();
	        } catch (Exception e) {
	        	Log.e("clickevent", e.getMessage());
			} finally {
				try {
					if (null != reader){
						reader.close();
					}
					dataSource.close();
				} catch (Exception e) {
					Log.i("clickevent", e.getMessage());
				}
			}
	        return null;
	    }
	}

	
	/**
	   * Sets default values for preferences. To be called the first time this app is run.
	   */
	  private void setDefaultPreferences() {
	    prefs = PreferenceManager.getDefaultSharedPreferences(this);

	    // Continuous preview
	    prefs.edit().putBoolean(PreferencesActivity.KEY_CONTINUOUS_PREVIEW, CaptureActivity.DEFAULT_TOGGLE_CONTINUOUS).commit();

	    // Recognition language
	    prefs.edit().putString(PreferencesActivity.KEY_SOURCE_LANGUAGE_PREFERENCE, CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE).commit();

	    // Translation
	    prefs.edit().putBoolean(PreferencesActivity.KEY_TOGGLE_TRANSLATION, CaptureActivity.DEFAULT_TOGGLE_TRANSLATION).commit();

	    // Translation target language
	    prefs.edit().putString(PreferencesActivity.KEY_TARGET_LANGUAGE_PREFERENCE, CaptureActivity.DEFAULT_TARGET_LANGUAGE_CODE).commit();

	    // Translator
	    prefs.edit().putString(PreferencesActivity.KEY_TRANSLATOR, CaptureActivity.DEFAULT_TRANSLATOR).commit();

	    // OCR Engine
	    prefs.edit().putString(PreferencesActivity.KEY_OCR_ENGINE_MODE, CaptureActivity.DEFAULT_OCR_ENGINE_MODE).commit();

	    // Autofocus
	    prefs.edit().putBoolean(PreferencesActivity.KEY_AUTO_FOCUS, CaptureActivity.DEFAULT_TOGGLE_AUTO_FOCUS).commit();
	    
	    // Beep
	    prefs.edit().putBoolean(PreferencesActivity.KEY_PLAY_BEEP, CaptureActivity.DEFAULT_TOGGLE_BEEP).commit();

	    // Character blacklist
	    prefs.edit().putString(PreferencesActivity.KEY_CHARACTER_BLACKLIST, 
	        OcrCharacterHelper.getDefaultBlacklist(CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE)).commit();

	    // Character whitelist
	    prefs.edit().putString(PreferencesActivity.KEY_CHARACTER_WHITELIST, 
	        OcrCharacterHelper.getDefaultWhitelist(CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE)).commit();

	    // Page segmentation mode
	    prefs.edit().putString(PreferencesActivity.KEY_PAGE_SEGMENTATION_MODE, CaptureActivity.DEFAULT_PAGE_SEGMENTATION_MODE).commit();

	    // Reversed camera image
	    prefs.edit().putBoolean(PreferencesActivity.KEY_REVERSE_IMAGE, CaptureActivity.DEFAULT_TOGGLE_REVERSED_IMAGE).commit();
	    
	    // Light
	    prefs.edit().putBoolean(PreferencesActivity.KEY_TOGGLE_LIGHT, CaptureActivity.DEFAULT_TOGGLE_LIGHT).commit();
	  }
	  
	  /**
	   * Displays an error message dialog box to the user on the UI thread.
	   * 
	   * @param title The title for the dialog box
	   * @param message The error message to be displayed
	   */
	  void showErrorMessage(String title, String message) {
		  new AlertDialog.Builder(this)
		    .setTitle(title)
		    .setMessage(message)
		    .setOnCancelListener(new FinishListener(this))
		    .setPositiveButton( "Done", new FinishListener(this))
		    .show();
	  }
}
