package com.wanderoo.ocr;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;

import com.wanderoo.database.DataSource;
import com.wanderoo.database.Location;


public class LocationIDDatabase {
	protected static String keyword;
	private static ArrayList<Location> locationList = new ArrayList<Location>();
	private static String randomText = "Microsoft TechTalk: For Graduate Students. This event will be held at SCD 101 at USC.";
	private static ArrayList<String> stopwords;
	private static Location matchedToName, matchedToAddress;
	private static Location matchedToCode;
	private Context ctx;

	LocationIDDatabase(String text, Context ctx) {
		LocationIDDatabase.randomText = text;
		this.ctx = ctx;
		LocationIDDatabase.locationList = getLocListAsArrayList();
	}

	private ArrayList<Location> getLocListAsArrayList() {
		DataSource dataSource = new DataSource(ctx);
		dataSource.open();
		try{
			return dataSource.getAllLocations();
		}finally{
			dataSource.close();
		}
	}

	public String checkLocation() {
		// File f = new File(file);
		stopwords = new ArrayList<String>();
		stopwords.add("this");
		stopwords.add("that");
		stopwords.add("in");
		stopwords.add("there");
		stopwords.add("here");

		// Place recognition.
		HashMap<String, Integer> placeScore = new HashMap<String, Integer>();
		String[] wordss, words;
		wordss = randomText.split("[ ,.\n-:]");
		ArrayList<String> arr = new ArrayList<String>();
		for(String str:wordss){
			if(str!=null && !str.equals(""))
			arr.add(str);
		}
		words = arr.toArray(new String[arr.size()]);
		int count = 0;
		String currentWord = null;
		while (count < words.length) {
			currentWord = words[count];
			if(currentWord!=null && !currentWord.equals("")){
				int score = 0;
	
				if (isALocationCode(currentWord)) {
					// Exact match for a building code fetches a score of 62 on a
					// scale of 0 to 100.
					if (placeScore
							.containsKey(matchedToCode.loc_addr.toLowerCase())) {
						score = placeScore.remove(currentWord) + 62;
					} else {
						score = 62;
					}
					placeScore.put(matchedToCode.loc_addr.toLowerCase(), score);
				} else {
					int nameScore = 0;
					if (isAPartialNameMatch(currentWord)) {
						nameScore = getPartialNameMatchScore(words, count);
					}
					int addressScore = 0;
					if (isAPartialAddressMatch(currentWord)) {
						addressScore = getPartialAddressMatchScore(words, count);
					}
					if (nameScore > 0 || addressScore > 0) {
						if (nameScore > addressScore) {
							placeScore.put(matchedToName.loc_addr.toLowerCase(),
									nameScore);
						} else {
							placeScore.put(matchedToAddress.loc_addr.toLowerCase(),
									addressScore);
						}
					}
				}
			}
			count++;
		}
		Map.Entry<String, Integer> mapEntry = null;
		for (Entry<String, Integer> entry : placeScore.entrySet()) {
			if(mapEntry==null || entry.getValue()>mapEntry.getValue())
		    mapEntry=entry;
		}
		return mapEntry.getKey();
	}

	private static int getPartialAddressMatchScore(String[] words, int count) {
		int j = count, i = 0;
		boolean matched = true;
		String[] locationSplit;
		locationSplit = matchedToAddress.loc_addr.split(" ");
		while (matched) {
			if (!words[j].equalsIgnoreCase(locationSplit[i])) {
				matched = false;
				break;
			} else {
				j++;
				i++;
			}
		}
		return (i * 100) / locationSplit.length;
	}

	private static int getPartialNameMatchScore(String[] words, int count) {
		int j = count, i,numberOfWordsMatched=0;
		boolean matched = true;
		String[] locationSplit;
		locationSplit = matchedToName.loc_name.split(" ");
		for(i=0;i<locationSplit.length-1;i++){
			if(locationSplit[i].equalsIgnoreCase(words[count])){
				break;
			}
		}
		while (matched && i<locationSplit.length && j<words.length) {
			if (!words[j].equalsIgnoreCase(locationSplit[i])) {
				matched = false;
				break;
			} else {
				j++;
				i++;
				numberOfWordsMatched++;
			}
		}
		return (numberOfWordsMatched * 100) / locationSplit.length;
	}

	private static boolean isAPartialAddressMatch(String currentWord) {
		for (int i = 0; i < locationList.size(); i++) {
			if (locationList.get(i).loc_addr.contains(currentWord)
					&& !isAStopWord(currentWord)) {
				matchedToAddress = locationList.get(i);
				return true;
			}
		}
		return false;
	}

	private static boolean isAStopWord(String currentWord) {
		return (stopwords.contains(currentWord));
	}

	private static boolean isAPartialNameMatch(String currentWord) {
		for (int i = 0; i < locationList.size(); i++) {
			if (locationList.get(i).loc_name.toLowerCase().contains(currentWord.toLowerCase())) {
				matchedToName = locationList.get(i);
				return true;
			}
		}
		return false;
	}

	private static boolean isALocationCode(String currentWord) {
		for (int i = 0; i < locationList.size(); i++) {
			if (currentWord.equalsIgnoreCase(locationList.get(i).loc_code)) {
				matchedToCode = locationList.get(i);
				return true;
			}
		}
		return false;
	}
}
