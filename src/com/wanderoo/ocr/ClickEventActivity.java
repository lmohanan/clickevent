package com.wanderoo.ocr;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.wanderoo.database.CalendarEvent;
import com.wanderoo.database.DataSource;

public class ClickEventActivity extends Activity {
	private Typeface tf;
	private static Context context;
	public static SimpleDateFormat format = new SimpleDateFormat("ddMMMyyyy", Locale.US);
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.US);
	private static String NUMBER_OF_EVENTS = "3";
	ViewFlipper flipper = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getApplicationContext();
		setContentView(R.layout.home_layout);
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		tf = Typeface
				.createFromAsset(getAssets(), "fonts/AVERIA_REGULAR.TTF");

		flipper = (ViewFlipper) findViewById(R.id.flipper);
	}
	
	protected void loadLatestEvents(){
		ArrayList<CalendarEvent> events = getCalendarEvents(NUMBER_OF_EVENTS);
		flipper.removeAllViews();
		if(events.size()>1)
			flipper.setAutoStart(true);
		else 
			flipper.setAutoStart(false);
			
		if(null!=events && events.size()>0){
			Calendar startDate = Calendar.getInstance();
			Calendar endDate = Calendar.getInstance();
			for(int i=0;i<events.size();i++){
				final CalendarEvent calEvent = events.get(i);
				startDate.setTimeInMillis(Long.valueOf(calEvent.start_date));
				endDate.setTimeInMillis(Long.valueOf(calEvent.end_date));
				
				LinearLayout event = (LinearLayout)getLayoutInflater().inflate(R.layout.homepage_event_layout, null);
				event.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
					      // We can read and write the media
							String file = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
									+ "/"
									+ calEvent.event_id + ".png";
						    try {
								Intent intent = new Intent();
								intent.setAction(Intent.ACTION_VIEW);
								intent.setDataAndType(
										Uri.parse("file://"
												+ file),
										"image/png");
								startActivity(intent);
						    } catch (NullPointerException e) {
						        // We get an error here if the SD card is visible, but full
						        Log.e("ClickEventActivity", "External storage is unavailable");
						        Toast.makeText(getApplicationContext(), "Required external storage (such as an SD card) is full or unavailable.",Toast.LENGTH_LONG).show();
						    }
					    }
					}
				});
				
				((TextView)event.findViewById(R.id.title)).setText(calEvent.event_title);
				((TextView)event.findViewById(R.id.date)).setText(dateFormat.format(startDate.getTime()));
				((TextView) event.findViewById(R.id.time))
						.setText(CreateEventActivity.timeFormat
								.format(startDate.getTime())
								+ " to "
								+ CreateEventActivity.timeFormat.format(endDate
										.getTime()));
				
				flipper.addView(event);
			}
		} else {
			LinearLayout event = (LinearLayout)getLayoutInflater().inflate(R.layout.homepage_event_layout, null);
			((TextView)event.findViewById(R.id.title)).setText("Your last 3 saved events come up here...");
			flipper.addView(event);
		}
	}
	public static ArrayList<CalendarEvent> getCalendarEvents(String number_of_events){
		DataSource dataSource = new DataSource(context);
		try {
			dataSource.open();
			return dataSource.getAllEvents(number_of_events);
		} catch (Exception e) {
			Log.e("clickevent", e.getMessage());
		} finally {
			try {
				dataSource.close();
			} catch (Exception e) {
				Log.i("clickevent", e.getMessage());
			}
		}
		return null;
	}
	@Override
	protected void onResume() {
		super.onResume();
		loadLatestEvents();
	}
	
	public void captureEvent(View v){
		startActivityForResult(new Intent(this, CaptureActivity.class), 0);
	}
	
	public void listEvents(View v){
		startActivity(new Intent(this,EventsListActivity.class));
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 0){
			if(data == null)
				return;
			String number="";
			StringBuilder result = new StringBuilder(data.getStringExtra("result"));
			//StringBuilder result = new StringBuilder("4 sep 2013");
			Pattern pattern = Pattern.compile("\\d{3}[-./]*\\d{3}[-./]*\\d{4}");
			Matcher matcher = pattern.matcher(result);
			if(matcher.find()){
				number = matcher.group();
			}
			String time = "";
			pattern = Pattern
					//9:00 am to 10:00 am
					.compile("((\\d{2}|\\d{1})[:/]*(\\d{2})[ ]*(a|A|p|P)[.,]*(m|M)[,. ]*(to|TO|_|-)*[ ]*(\\d{2}|\\d{1})[:/]*(\\d{2})[ ]*(a|A|P|p)[,.]*(m|M)[.]*)" +
							"|" +
							//9:00 am
							"((\\d{2}|\\d{1})[:/]*(\\d{2})[ ]*(a|A|p|P)[.,]*(m|M)[,.]*)" +
							"|" +
							//7am
							"((\\d{2}|\\d{1})[ ]*(a|A|p|P)[.,]*(m|M)[,.]*)");
			matcher = pattern.matcher(result);
			if(matcher.find()){
				time = matcher.group();
		        time = time.toUpperCase().replaceAll("[\\s]+", " ").replaceAll("TO|_", "-").replaceAll("/", ":").replaceAll(",",".").replaceAll("A.M.", "AM").replaceAll("P.M.", "PM");
			}
			String email = "";
			pattern = Pattern.compile("[a-zA-Z]+[0-9]*[.]*@[a-zA-Z]+[0-9]*.[a-zA-Z]+[0-9]*");
			matcher = pattern.matcher(result);
			if(matcher.find()){
				email = matcher.group();
			}
			String date = "";
			pattern = Pattern.compile("(?i)((" +
					//01-01-2013 or 1-1-13
					"(\\d{1}|\\d{2})[-/:,]+(\\d{1}|\\d{2})[-/:,]+(\\d{4}|\\d{2}))" +
					"|" +
					//Jan 1,2013 or January 1, 2013
					"((January|Jan|February|Feb|March|Mar|April|Apr|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)"+
					"(,|.| |-|/|)*(\\d{1}|\\d{2})(st|rd|nd|th)*(,|.| |-|/|)*(\\d{4}))" +
					"|" +
					//1-Jan-2013
					"((\\d{1}|\\d{2})" +
					"[-/:,]+" +
					"(January|Jan|February|Feb|March|Mar|April|Apr|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)"+
					"[-/:,]+" +
					"(\\d{4}|\\d{2}))" +
					"|" +
					//1 Jan 2013
					"((\\d{1}|\\d{2})" +
					"( )+" +
					"(January|Jan|February|Feb|March|Mar|April|Apr|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)" +
					"( )+" +
					"(\\d{4}|\\d{2}))" +
					")"
					);
			matcher = pattern.matcher(result);
			if(matcher.find()){
				date = matcher.group();
				date = date.replaceAll("[\\s-_]+", " ");
			} else {
				pattern = Pattern.compile("(?i)");
				matcher= pattern.matcher(result);
				if(matcher.find()){
					date = matcher.group();
				}
			}
			
			String location = new LocationID(result.toString(),getApplicationContext()).checkLocation();
			
			//((TextView)findViewById(R.id.result)).setText(result+number+time+email+date+location);
			Intent eventIntent = new Intent(getApplicationContext(),CreateEventActivity.class);
			eventIntent.putExtra("result", result.toString());
			eventIntent.putExtra("number", number);
			eventIntent.putExtra("time", time);
			eventIntent.putExtra("email", email);
			eventIntent.putExtra("date", date);
			eventIntent.putExtra("location", location);
			eventIntent.putExtra("eventImage", data.getParcelableExtra("eventImage"));
			startActivity(eventIntent);
		} else {
			finish();
		}
	}
}
