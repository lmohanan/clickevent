package com.wanderoo.ocr;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;

import com.wanderoo.database.DataSource;
import com.wanderoo.database.Location;


public class LocationID {
	protected static String keyword;
	private static ArrayList<Location> locationList = new ArrayList<Location>();
	private static String randomText = "";
	private static ArrayList<String> stopwords;
	private static ArrayList<Location> matchedToName=new ArrayList<Location>();//, matchedToAddress=new ArrayList<Location>();
	private static Location matchedToCode;
	private Context ctx;

	LocationID(String text, Context ctx) {
		randomText = text;
		this.ctx = ctx;
		locationList = getLocListAsArrayList();
	}

	private ArrayList<Location> getLocListAsArrayList() {
		DataSource dataSource = new DataSource(ctx);
		dataSource.open();
		try{
			return dataSource.getAllLocations();
		}finally{
			dataSource.close();
		}
	}

	static HashMap<String, Integer> placeScore = new HashMap<String, Integer>();

	public String checkLocation() {
		if(locationList.size()>0)
		if(randomText.length()>0){
			placeScore.clear();
			// File f = new File(file);
			stopwords = new ArrayList<String>();
			stopwords.add("this");
			stopwords.add("that");
			stopwords.add("in");
			stopwords.add("there");
			stopwords.add("here");
	
			// Place recognition.
			String[] wordss, words;
			wordss = randomText.split("[ ,.\n-:]");
			ArrayList<String> arr = new ArrayList<String>();
			for(String str:wordss){
				if(str!=null && !str.equals(""))
				arr.add(str);
			}
			words = arr.toArray(new String[arr.size()]);
			int count = 0;
			String currentWord = null;
			while (count < words.length) {
				currentWord = words[count];
				if(currentWord!=null && !currentWord.equals("")){
					int score = 0;
		
					if (isALocationCode(currentWord)) {
						// Exact match for a building code fetches a score of 62 on a
						// scale of 0 to 100.
						if (placeScore
								.containsKey(matchedToCode.loc_addr.toLowerCase())) {
							score = placeScore.remove(currentWord) + 62;
						} else {
							score = 62;
						}
						placeScore.put(matchedToCode.loc_addr.toLowerCase(), score);
					} else {
	//					matchedToAddress.clear();
						matchedToName.clear();
						if (isAPartialNameMatch(currentWord)) {
							getPartialNameMatchScore(words, count);
						}
						/*int addressScore = 0;
						if (isAPartialAddressMatch(currentWord)) {
							addressScore = getPartialAddressMatchScore(words, count);
						}*/
						/*if (nameScore > 0 || addressScore > 0) {
							if (nameScore > addressScore) {
								placeScore.put(matchedToName.loc_addr.toLowerCase(),
										nameScore);
							} else {
								placeScore.put(matchedToAddress.loc_addr.toLowerCase(),
										addressScore);
							}
						}*/
					}
				}
				count++;
			}
			Map.Entry<String, Integer> mapEntry = null;
			for (Entry<String, Integer> entry : placeScore.entrySet()) {
				if(mapEntry==null || entry.getValue()>mapEntry.getValue())
			    mapEntry=entry;
			}
			return (null==mapEntry?null:mapEntry.getKey());
		}
		return null;
	}

	/*private static int getPartialAddressMatchScore(String[] words, int count) {
		int j = count, i = 0;
		boolean matched = true;
		String[] locationSplit;
		locationSplit = matchedToAddress.loc_addr.split(" ");
		while (matched) {
			if (!words[j].equalsIgnoreCase(locationSplit[i])) {
				matched = false;
				break;
			} else {
				j++;
				i++;
			}
		}
		return (i * 100) / locationSplit.length;
	}*/

	private static void getPartialNameMatchScore(String[] words, int count) {
		int j, i,numberOfWordsMatched;
		boolean matched;
		int nameScore;
		for(Location matchedLocn:matchedToName){
			matched = true;
			j = count;
			numberOfWordsMatched=0;
			String[] locationSplit;
			nameScore = 0;
			locationSplit = matchedLocn.loc_name.split(" ");
			for(i=0;i<locationSplit.length-1;i++){
				if(locationSplit[i].equalsIgnoreCase(words[count])){
					break;
				}
			}
			while (matched && i<locationSplit.length && j<words.length) {
				if (!words[j].equalsIgnoreCase(locationSplit[i])) {
					matched = false;
					break;
				} else {
					j++;
					i++;
					numberOfWordsMatched++;
				}
			}
			nameScore = (numberOfWordsMatched * 100) / locationSplit.length;
			if(nameScore > 0){
				if(placeScore.containsKey(matchedLocn.loc_name.toLowerCase())){
					int score = placeScore.get(matchedLocn.loc_name.toLowerCase());
					if(score<nameScore){
						placeScore.put(matchedLocn.loc_name.toLowerCase(), nameScore);
					}
				} else {
					placeScore.put(matchedLocn.loc_name.toLowerCase(),
							nameScore);
				}
			}
		}
	}

	/*private static boolean isAPartialAddressMatch(String currentWord) {
		for (int i = 0; i < locationList.size(); i++) {
			if (locationList.get(i).loc_addr.contains(currentWord)
					&& !isAStopWord(currentWord)) {
				matchedToAddress.add(locationList.get(i));
			}
		}
		return matchedToAddress.size()>0;
	}*/

	private static boolean isAStopWord(String currentWord) {
		return (stopwords.contains(currentWord));
	}

	private static boolean isAPartialNameMatch(String currentWord) {
		for (int i = 0; i < locationList.size(); i++) {
			if (locationList.get(i).loc_name.toLowerCase().contains(currentWord.toLowerCase())) {
				matchedToName.add(locationList.get(i));
			}
		}
		return matchedToName.size()>0;
	}

	private static boolean isALocationCode(String currentWord) {
		for (int i = 0; i < locationList.size(); i++) {
			if (currentWord.equalsIgnoreCase(locationList.get(i).loc_code)) {
				matchedToCode = locationList.get(i);
				return true;
			}
		}
		//get all locations tht match
		return false;
	}
}

/*
for each word
check if it is a code
if not check if it partially matches any locations - return all partially matched loc names ke locs
for each loc name that matched, get a score
if nothing found till now repeat what we did for name for addr
*/