package com.wanderoo.ocr;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class EventsListArrayAdaptor extends ArrayAdapter<String> {
	private EventsListActivity eventsListActivity;

	public EventsListArrayAdaptor(EventsListActivity eventsListActivity,
			ArrayList<String> resources) {
		super(eventsListActivity, R.layout.homepage_event_layout, resources);
		this.eventsListActivity = eventsListActivity;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) eventsListActivity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout rowView = (LinearLayout)inflater.inflate(R.layout.homepage_event_layout, parent,
				false);
		rowView.setBackgroundResource(R.drawable.list_event_background);

		rowView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			      // We can read and write the media
					String file = eventsListActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
							+ "/"
							+ eventsListActivity.events
							.get(position).event_id + ".png";
				    try {
						Intent intent = new Intent();
						intent.setAction(Intent.ACTION_VIEW);
						intent.setDataAndType(
								Uri.parse("file://"
										+ file),
								"image/png");
						eventsListActivity.startActivity(intent);
				    } catch (NullPointerException e) {
				        // We get an error here if the SD card is visible, but full
				        Log.e("ClickEventActivity", "External storage is unavailable");
				        Toast.makeText(eventsListActivity, "Required external storage (such as an SD card) is full or unavailable.",Toast.LENGTH_LONG).show();
				    }
			    }
			}
		});
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();

		startDate.setTimeInMillis(Long.valueOf(eventsListActivity.events
				.get(position).start_date));
		endDate.setTimeInMillis(Long.valueOf(eventsListActivity.events
				.get(position).end_date));

		((TextView) rowView.findViewById(R.id.title)).setTextSize(
				TypedValue.COMPLEX_UNIT_SP, 18);
		((TextView) rowView.findViewById(R.id.title))
				.setText(eventsListActivity.events.get(position).event_title);

		((TextView) rowView.findViewById(R.id.date)).setTextSize(
				TypedValue.COMPLEX_UNIT_SP, 12);
		((TextView) rowView.findViewById(R.id.date))
				.setText(ClickEventActivity.dateFormat.format(startDate
						.getTime()));

		((TextView) rowView.findViewById(R.id.time)).setTextSize(
				TypedValue.COMPLEX_UNIT_SP, 12);
		((TextView) rowView.findViewById(R.id.time))
				.setText(CreateEventActivity.timeFormat.format(startDate
						.getTime())
						+ " to "
						+ CreateEventActivity.timeFormat.format(endDate
								.getTime()));

		return rowView;
	}
}