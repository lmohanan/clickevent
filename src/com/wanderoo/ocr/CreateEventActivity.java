package com.wanderoo.ocr;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.wanderoo.database.DataSource;

public class CreateEventActivity extends Activity {
	private final static String[] DT_FORMATS = { "MM-dd-yyyy", "MMM dd,yyyy",
			"dd-MMM-yyyy", "dd-MM-yyyy", "dd-MM-yy", "MM-dd-yy", "M-d-yy",
			"d-M-yy", "y-M-d", "y-d-M", "d MMM yy", "dd MMM yyyy" };
	private static final String TAG = "CreateEventActivity";
	private String result;
	private String email;
	private String number;
	private String location;
	private String date;
	private String time;
	private String title;
	private StringBuilder description = new StringBuilder();
	private static Calendar startDate = null;
	private static Calendar endDate = null;
	private Bitmap eventImage = null;

	private Spinner spinner = null;
	private static Button START_DATE_BTN;
	private static Button START_TIME_BTN;
	private static Button END_DATE_BTN;
	private static Button END_TIME_BTN;
	
	private Cursor cursor = null;
	private static ContentValues values = new ContentValues();
	private ContentResolver cr = null;

	private Map<String, Integer> availableCalendars = new HashMap<String, Integer>();
	private ArrayList<String> list = new ArrayList<String>();
	
	protected static SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.US);
	protected static SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a", Locale.US);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		values.clear();
		setContentView(R.layout.event_layout);
		cr = getContentResolver();

		result = getIntent().getStringExtra("result");
		eventImage = (Bitmap)getIntent().getParcelableExtra("eventImage");
		
		email = getIntent().getStringExtra("email");
		date = getIntent().getStringExtra("date");
		time = getIntent().getStringExtra("time");
		location = getIntent().getStringExtra("location");
		number = getIntent().getStringExtra("number");

		startDate = stringToStartTime(date, time, true);
		endDate = stringToStartTime(date, time, false);
		
		title = getEventTitle();
		if (title == null || title.equals(""))
			title = result.split("\n")[0];

		spinner = (Spinner) findViewById(R.id.calendars);
		getCalendars();

		((EditText) findViewById(R.id.title)).setText(title);
		((EditText) findViewById(R.id.email)).setText(email);
		((EditText) findViewById(R.id.number)).setText(number);
		((EditText) findViewById(R.id.location)).setText(location);
		
		START_DATE_BTN = ((Button)findViewById(R.id.startDate));
		START_DATE_BTN.setText(dateFormat.format(startDate.getTime()));
		
		START_TIME_BTN = ((Button)findViewById(R.id.startTime));
		START_TIME_BTN.setText(timeFormat.format(startDate.getTime()));
		
		END_DATE_BTN = ((Button)findViewById(R.id.endDate));
		END_DATE_BTN.setText(dateFormat.format(endDate.getTime()));
		
		END_TIME_BTN = ((Button)findViewById(R.id.endTime));
		END_TIME_BTN.setText(timeFormat.format(endDate.getTime()));
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getApplicationContext(), R.layout.spinner_textview, list);
		spinner.setAdapter(adapter);
		
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				values.put(Events.CALENDAR_ID, availableCalendars
						.get((String) spinner.getItemAtPosition(position)));
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		values.put(Events.DTSTART, startDate.getTimeInMillis());
		values.put(Events.DTEND, endDate.getTimeInMillis());
		values.put(Events.EVENT_TIMEZONE, "America/Los_Angeles");
	}
	
	public void setStartTime(View view) {
		TimePickerFragment.START_TIME = true;
		DialogFragment newFragment = new TimePickerFragment();
		newFragment.show(getFragmentManager(), "timePicker");
	}

	public void setEndTime(View view) {
		TimePickerFragment.START_TIME = false;
		DialogFragment newFragment = new TimePickerFragment();
		newFragment.show(getFragmentManager(), "timePicker");
	}

	public void setStartDate(View view) {
		DatePickerFragment.START_DATE = true;
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}

	public void setEndDate(View view) {
		DatePickerFragment.START_DATE = false;
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getFragmentManager(), "datePicker");
	}
	
	private void getCalendars() {
		// get available calendar id
		cursor = cr
				.query(Uri.parse("content://com.android.calendar/calendars"),
						new String[] { Calendars._ID,
								Calendars.CALENDAR_DISPLAY_NAME, Calendars.VISIBLE }, null, null,
						null);
		list = new ArrayList<String>();

		if (cursor.moveToNext()) {
			if(cursor.getInt(2)==1){
				list.add(cursor.getString(1));
				availableCalendars.put(cursor.getString(1), cursor.getInt(0));
				values.put(Events.CALENDAR_ID, cursor.getInt(0));
			}
		}

		while (cursor.moveToNext()) {
			if(cursor.getInt(2)==1){
				list.add(cursor.getString(1));
				availableCalendars.put(cursor.getString(1), cursor.getInt(0));
			}
		}
		cursor.close();
	}

	/** Method to convert a String to a Calendar date. */
	public static Calendar stringToStartTime(String date, String time,
			boolean startTime) throws ParseException {
		String dtFormat;
		Calendar cal = Calendar.getInstance();
		Date d = new Date();
		for (int i = 0; i < DT_FORMATS.length; i++) {
			dtFormat = DT_FORMATS[i];
			try {
				d = new SimpleDateFormat(dtFormat).parse(date);
				break;
			} catch (Exception e) {
			}
		}
		cal.setTime(d);
		try {
			// if time was detected
			if (time.length() > 0) {
				String times[] = null;

				if (time.contains("-")) {
					if (startTime) {
						// take the start time
						time = time.split("-")[0];
					} else {
						// take the start time
						time = time.split("-")[1];
					}
				}
				// remove spaces from beginning and end
				time = time.trim();
				// separate 9:00 AM into 9:00 and AM
				if (time.contains(" ")) {
					times = time.split(" ");
				} else {
					times = new String[2];
					times[0] = time.substring(
							0,
							time.contains("AM") ? time.indexOf("AM") : time
									.indexOf("PM"));
					times[1] = time.contains("AM") ? "AM" : "PM";
				}
				cal.set(Calendar.AM_PM, times[1].equals("AM") ? 0 : 1);
				if (times[0].contains(":")) {
					times = times[0].split(":");
					cal.set(Calendar.HOUR, Integer.valueOf(times[0]));
					cal.set(Calendar.MINUTE, Integer.valueOf(times[1]));
				} else {
					cal.set(Calendar.HOUR, Integer.valueOf(times[0]));
				}
			}
		} catch (Exception e) {
			// do nothing
		}
		return cal;
	}

	private String getEventTitle() {
		String[] lines = result.split("\n");
		// number
		Pattern pattern = Pattern
				.compile("(?i)(\\d{3}[-./]*\\d{3}[-./]*\\d{4})"
						+ "|"
						+ "((\\d{2}|\\d{1})[:/]*(\\d{2})[ ]*(a|A|p|P)[.,]*(m|M)[,. ]*(to|TO|_|-)*[ ]*(\\d{2}|\\d{1})[:/]*(\\d{2})[ ]*(a|A|P|p)[,.]*(m|M)[.]*)"
						+ "|"
						+
						// 9:00 am
						"((\\d{2}|\\d{1})[:/]*(\\d{2})[ ]*(a|A|p|P)[.,]*(m|M)[,.]*)"
						+ "|"
						+
						// 7am
						"((\\d{2}|\\d{1})[ ]*(a|A|p|P)[.,]*(m|M)[,.]*)"
						+ "|"
						+
						// email
						"[a-zA-Z]+[0-9]*[.]*@[a-zA-Z]+[0-9]*.[a-zA-Z]+[0-9]*"
						+ "|"
						+ "("
						+
						// 01-01-2013 or 1-1-13
						"(\\d{1}|\\d{2})[-/:,]+(\\d{1}|\\d{2})[-/:,]+(\\d{4}|\\d{2}))"
						+ "|"
						+
						// Jan 1,2013 or January 1, 2013
						"((" +
						//01-01-2013 or 1-1-13
						"(\\d{1}|\\d{2})[-/:,]+(\\d{1}|\\d{2})[-/:,]+(\\d{4}|\\d{2}))" +
						"|" +
						//Jan 1,2013 or January 1, 2013
						"((January|Jan|February|Feb|March|Mar|April|Apr|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)"+
						"(,|.| |-|/|)*(\\d{1}|\\d{2})(st|rd|nd|th)*(,|.| |-|/|)*(\\d{4}))" +
						"|" +
						//1-Jan-2013
						"((\\d{1}|\\d{2})" +
						"[-/:,]+" +
						"(January|Jan|February|Feb|March|Mar|April|Apr|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)"+
						"[-/:,]+" +
						"(\\d{4}|\\d{2}))" +
						"|" +
						//1 Jan 2013
						"((\\d{1}|\\d{2})" +
						"( )+" +
						"(January|Jan|February|Feb|March|Mar|April|Apr|May|June|Jun|July|Jul|August|Aug|September|Sep|October|Oct|November|Nov|December|Dec)" +
						"( )+" +
						"(\\d{4}|\\d{2}))" +
						")");
		Matcher matcher = null;

		for (String line : lines) {
			if (!line.equals("")) {
				matcher = pattern.matcher(line);
				if (!matcher.find()) {
					return line;
				}
			}
		}
		return null;
	}

	public void saveEvent(View view) {
		FileOutputStream out = null;
		title = ((EditText) findViewById(R.id.title)).getText().toString();
		email = ((EditText) findViewById(R.id.email)).getText().toString();
		number = ((EditText) findViewById(R.id.number)).getText().toString();
		location = ((EditText) findViewById(R.id.location)).getText()
				.toString();
		description.append(((EditText) findViewById(R.id.description)).getText()
				.toString()+"\n");

		values.put(Events.TITLE, title);
		if (!email.equals(""))
			description.append("Email: "+email+"\n");
		if (!number.equals(""))
			description.append("Contact Number: "+number+"\n");
		if (null != location && !location.equals(""))
			values.put(Events.EVENT_LOCATION, location);
		
		//get external directory location
		File eventsLocation = getStorageDirectory();
	    if (eventsLocation == null || (!eventsLocation.exists() && !eventsLocation.mkdirs())) {
	        Log.e(TAG, "Couldn't make directory " + eventsLocation);
			Toast.makeText(
					getApplicationContext(),
					"Couldn't make directory " + eventsLocation
							+ " to save event file", Toast.LENGTH_LONG).show();
	        return;
	    }
	    //insert the event in the selected calendar
		Uri uri = cr.insert(Events.CONTENT_URI, values);
		insertEvent(uri.getLastPathSegment(), title,
				Long.toString(startDate.getTimeInMillis()),
				Long.toString(endDate.getTimeInMillis()));

		try {
			//save the clicked image on disk
			out = new FileOutputStream(new File(eventsLocation,uri.getLastPathSegment()+".png"));
			eventImage.compress(Bitmap.CompressFormat.PNG, 90, out);
			out.close();
		} catch (Exception e) {
			Log.e(TAG, "Exception: "+e.getMessage());
	        Toast.makeText(getApplicationContext(), "Couldn't save event file to disk", Toast.LENGTH_LONG).show();
	        return;
		}

		String[] proj = new String[] { Events.TITLE };

		String selection = "(" + Events._ID + " = ? OR " + Events.TITLE
				+ " = ?)";

		String[] selectionArgs = new String[] { uri.getLastPathSegment(), title };

		// check if event was actually saved
		cursor = getContentResolver().query(Events.CONTENT_URI, proj,
				selection, selectionArgs, null);

		if (cursor.getCount() > 0) {
			System.out.println("CalendarEvent created");
		} else {
			System.out.println("CalendarEvent not created");
		}
		cursor.close();
		
		Toast.makeText(getApplicationContext(), "CalendarEvent saved successfully...", Toast.LENGTH_LONG).show();
		
		finish();
	}
	
	private void insertEvent(String id, String title, String startDate, String endDate){
		DataSource dataSource = new DataSource(getApplicationContext());
	        try {
	            dataSource.open();
	        	dataSource.createEvent(id, title, startDate, endDate);
	        } catch (Exception e) {
	        	Log.e("clickevent", e.getMessage());
			} finally {
				try {
					dataSource.close();
				} catch (Exception e) {
					Log.i("clickevent", e.getMessage());
				}
			}
	}

	public void cancel(View view) {
		finish();
	}
	
	  private File getStorageDirectory() {
	    String state = null;
	    try {
	      state = Environment.getExternalStorageState();
	    } catch (RuntimeException e) {
	      Log.e(TAG, "Is the SD card visible?", e);
	      Toast.makeText(getApplicationContext(), "Required external storage (such as an SD card) is unavailable.",Toast.LENGTH_LONG).show();
	    }
	    
	    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
	      // We can read and write the media
	      try {
	        return getExternalFilesDir(Environment.DIRECTORY_PICTURES);
	      } catch (NullPointerException e) {
	        // We get an error here if the SD card is visible, but full
	        Log.e(TAG, "External storage is unavailable");
	        Toast.makeText(getApplicationContext(), "Required external storage (such as an SD card) is full or unavailable.",Toast.LENGTH_LONG).show();
	      }
	    } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
	    	// We can only read the media
	    	Log.e(TAG, "External storage is read-only");
	    	Toast.makeText(getApplicationContext(), "Required external storage (such as an SD card) is unavailable for data storage.",Toast.LENGTH_LONG).show();
	    } else {
	    	// Something else is wrong. It may be one of many other states, but all we need
	      // to know is we can neither read nor write
	    	Log.e(TAG, "External storage is unavailable");
	    	Toast.makeText(getApplicationContext(), "Required external storage (such as an SD card) is unavailable or corrupted.",Toast.LENGTH_LONG).show();
	    }
	    return null;
	  }

	public static class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {
		protected static boolean START_TIME;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			
			int hour = 0;
			int minute = 0;
			if(START_TIME) {
				hour = startDate.get(Calendar.HOUR_OF_DAY);
				minute = startDate.get(Calendar.MINUTE);
			} else {
				hour = endDate.get(Calendar.HOUR_OF_DAY);
				minute = endDate.get(Calendar.MINUTE);
			}

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			if (START_TIME) {
				startDate.set(Calendar.HOUR, hourOfDay);
				startDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
				startDate.set(Calendar.MINUTE, minute);
				
				START_TIME_BTN.setText(timeFormat.format(startDate.getTime()));
				values.put(Events.DTSTART, startDate.getTimeInMillis());
			} else {
				endDate.set(Calendar.HOUR, hourOfDay);
				endDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
				endDate.set(Calendar.MINUTE, minute);

				END_TIME_BTN.setText(timeFormat.format(endDate.getTime()));
				values.put(Events.DTEND, endDate.getTimeInMillis());
			}
		}
	}
	
	public static class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {
		protected static boolean START_DATE;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			int year, month, day;
			if (START_DATE) {
				year = startDate.get(Calendar.YEAR);
				month = startDate.get(Calendar.MONTH);
				day = startDate.get(Calendar.DAY_OF_MONTH);
			} else {
				year = endDate.get(Calendar.YEAR);
				month = endDate.get(Calendar.MONTH);
				day = endDate.get(Calendar.DAY_OF_MONTH);
			}
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			if (START_DATE) {
				startDate.set(Calendar.YEAR, year);
				startDate.set(Calendar.MONTH, month);
				startDate.set(Calendar.DAY_OF_MONTH, day);
				values.put(Events.DTSTART, startDate.getTimeInMillis());
				START_DATE_BTN.setText(dateFormat.format(startDate.getTime()));
			} else {
				endDate.set(Calendar.YEAR, year);
				endDate.set(Calendar.MONTH, month);
				endDate.set(Calendar.DATE, day);
				values.put(Events.DTEND, endDate.getTimeInMillis());
				END_DATE_BTN.setText(dateFormat.format(endDate.getTime()));
			}
		}
	}
}