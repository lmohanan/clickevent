package com.wanderoo.appspot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.wanderoo.ocr.ClickEventActivity;
import com.wanderoo.ocr.R;

public class GetUniversities extends Activity {
	ListView univList = null;
	ProgressDialog mProgressDialog = null;
	ArrayList<String> univs = new ArrayList<String>();
	ArrayAdapter<String> adapter = null;
	private ArrayList<String> univAbbreviations = new ArrayList<String>();
	int selectedPosition = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setFinishOnTouchOutside(false);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.select_university);
		univList = (ListView) findViewById(R.id.univList);
		univs.clear();
		univs.add("I am not a student");
		univAbbreviations.add("notstudent");
		adapter = new ArrayAdapter<String>(this, R.layout.univ_row, univs);
		univList.setAdapter(adapter);
		univList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long id) {
				selectedPosition=position;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		mProgressDialog = new ProgressDialog(GetUniversities.this);
		mProgressDialog.setMessage("Loading...");
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialog.show();
	}

	private class DownloadFile extends AsyncTask<String, Integer, String> {
		BufferedReader reader = null;
		String line = null;

		@Override
		protected String doInBackground(String... sUrl) {
			try {
				HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet();
				request.setURI(new URI(sUrl[0]));
				HttpResponse response = client.execute(request);
				reader = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				//first line is number of locations
				reader.readLine();
				while ((line = reader.readLine()) != null) {
					Log.i("university", line);
					String str[] = line.split("\\|");
					univs.add(str[0]);
					univAbbreviations.add(str[1]);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (null != reader)
						reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(mProgressDialog.isShowing())
				mProgressDialog.dismiss();
			runOnUiThread(new Runnable() {
				public void run() {
					adapter.notifyDataSetChanged();
				}
			});

		}
	}
	
	public void selectUniv(View v){
		Intent result = new Intent();
		result.putExtra("univ", univAbbreviations.get(selectedPosition));
		setResult(RESULT_OK,result);
		finish();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		setResult(RESULT_CANCELED);
		finish();
	}

}
