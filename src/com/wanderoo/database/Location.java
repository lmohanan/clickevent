package com.wanderoo.database;

public class Location {
	public String loc_code;
	public String loc_name;
	public String loc_addr;

	public Location(String code, String name, String addr) {
		loc_code = code;
		loc_name = name;
		loc_addr = addr;
	}
}
