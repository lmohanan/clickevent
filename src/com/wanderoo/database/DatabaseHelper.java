package com.wanderoo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	public static final String TABLE_LOCATIONS = "locations";
	public static final String TABLE_EVENTS = "events";
	
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_CODE = "code";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_ADDRESS = "address";
	
	public static final String COLUMN_EVENT_TITLE = "event_title";
	public static final String COLUMN_EVENT_START_DATE = "event_start_date";
	public static final String COLUMN_EVENT_END_DATE = "event_end_date";

	private static final String DATABASE_NAME = "clickevent.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String CREATE_LOCATIONS = "create table "
			+ TABLE_LOCATIONS + "("
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_CODE + " text not null, "
			+ COLUMN_NAME + " text, "
			+ COLUMN_ADDRESS + " text);";
	
	private static final String CREATE_EVENTS = "create table " +
			TABLE_EVENTS + "(" +
			COLUMN_ID + " text primary key," +
			COLUMN_EVENT_TITLE + " text," +
			COLUMN_EVENT_START_DATE + " text," +
			COLUMN_EVENT_END_DATE + " text);";
	
	private static String DROP_TABLE = "DROP TABLE IF EXISTS ";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(CREATE_LOCATIONS);
		database.execSQL(CREATE_EVENTS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DatabaseHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL(DROP_TABLE + TABLE_LOCATIONS);
		db.execSQL(DROP_TABLE + TABLE_EVENTS);
		onCreate(db);
	}
}
