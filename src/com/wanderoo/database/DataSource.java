package com.wanderoo.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DataSource {
	private SQLiteDatabase database;
	private DatabaseHelper helper;
	
	private static String[] location_columns = { DatabaseHelper.COLUMN_CODE,
			DatabaseHelper.COLUMN_NAME, DatabaseHelper.COLUMN_ADDRESS };

	private static String[] event_columns = { DatabaseHelper.COLUMN_ID,
			DatabaseHelper.COLUMN_EVENT_TITLE,
			DatabaseHelper.COLUMN_EVENT_START_DATE,
			DatabaseHelper.COLUMN_EVENT_END_DATE };

	public DataSource(Context context) {
		helper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		database = helper.getWritableDatabase();
	}

	public void close() {
		helper.close();
	}

	public boolean createLocation(String code, String name, String address) {
		try {
			ContentValues values = new ContentValues();
			values.put(DatabaseHelper.COLUMN_CODE, code);
			values.put(DatabaseHelper.COLUMN_NAME, address);
			values.put(DatabaseHelper.COLUMN_ADDRESS, name);
			database.insert(DatabaseHelper.TABLE_LOCATIONS, null, values);
		} catch (Exception e) {
			Log.e("clickevent", e.getMessage());
			return false;
		}
		return true;
	}
	
	public boolean createEvent(String id, String title, String startDate, String endDate){
		try {
			ContentValues values = new ContentValues();
			values.put(DatabaseHelper.COLUMN_ID, id);
			values.put(DatabaseHelper.COLUMN_EVENT_TITLE, title);
			values.put(DatabaseHelper.COLUMN_EVENT_START_DATE, startDate);
			values.put(DatabaseHelper.COLUMN_EVENT_END_DATE, endDate);
			database.insert(DatabaseHelper.TABLE_EVENTS, null, values);
		} catch (Exception e) {
			Log.e("clickevent", e.getMessage());
			return false;
		}
		return true;
	}

	public ArrayList<Location> getAllLocations() {
		ArrayList<Location> locations = new ArrayList<Location>();

		Cursor cursor = database.query(DatabaseHelper.TABLE_LOCATIONS,
				location_columns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Location location = new Location(cursor.getString(0), cursor.getString(2),
					cursor.getString(1));
			locations.add(location);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return locations;
	}
	
	public ArrayList<CalendarEvent> getAllEvents(String limit) {
		ArrayList<CalendarEvent> events = new ArrayList<CalendarEvent>();
		
		Cursor cursor = database.query(DatabaseHelper.TABLE_EVENTS,
				event_columns, null, null, null, null, event_columns[0]
						+ " DESC", limit);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			CalendarEvent event = new CalendarEvent(cursor.getString(0),
					cursor.getString(1), cursor.getString(2),
					cursor.getString(3));
			events.add(event);
			cursor.moveToNext();
		}
		cursor.close();
		return events;
	}
}
