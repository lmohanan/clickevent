package com.wanderoo.database;

public class CalendarEvent {
	public String event_id;
	public String event_title;
	public String end_date;
	public String start_date;

	public CalendarEvent(String id, String title, String start_date, String end_date) {
		event_id = id;
		event_title = title;
		this.end_date = end_date;
		this.start_date = start_date;
	}
}
